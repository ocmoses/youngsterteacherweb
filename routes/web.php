<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/about', function(){
	return view('about');
})->name('about');



Route::group(['prefix' => 'dashboard'], function(){

	Route::get('/', 'DashboardController@index')->name('dashboard');
	Route::get('/attendance', 'DashboardController@index')->name('attendance');
	Route::get('/broadcast', 'DashboardController@index')->name('broadcast');
	Route::get('/chats', 'DashboardController@index')->name('chats');
	Route::get('/child-location', 'DashboardController@index')->name('child-location');
	Route::get('/digital-homework', 'DashboardController@index')->name('digital-homework');
	Route::get('/inbox', 'DashboardController@index')->name('inbox');
	Route::get('/select-child', 'DashboardController@index')->name('select-child');
	Route::get('/subject-time-table', 'DashboardController@index')->name('subject-time-table');
});
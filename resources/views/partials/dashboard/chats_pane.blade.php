<div class="item-content row" id="chats-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane row" >
						
			<div class="col-md-5 col-lg-5 select-chat">
				<h5 class="header" >Teacher</h5>
				<div class="chat-item">
					<div class="initials">
						<p>KB</p>
					</div>
					<div class="last-message">
						<h6>Mr. John Doe</h6>
						<p>Hello, what's up? I want a school fees refund</p>
					</div>
					<div class="next">
						<i class="fas fa-chevron-right"></i>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-lg-7 chat">
				<h5 class="header" >Mr. John Doe</h5>
				<div id="messages-container">

					<p class="message-time">Yesterday 12:20 a.m.</p>
					<div class="in-message ml-auto">
						<div class="initials">
							<p>KB</p>
						</div>
						<div class="message-body">
							<p>Why did your child say you can't allow him to fight?</p>
						</div>
					</div>

					<p class="message-time">Yesterday 12:25 a.m.</p>
					<div class="out-message">
						<div class="initials">
							<p>AA</p>
						</div>
						<div class="message-body">
							<p>I am a peaceful person. If you want to fight, go and do heavy weight boxing.</p>
						</div>
					</div>

					<p class="message-time">Yesterday 12:30 a.m.</p>
					<div class="in-message ml-auto">
						<div class="initials">
							<p>KB</p>
						</div>
						<div class="message-body">
							<p>In my class, people must fight to survive. You know it is a jungle.</p>
						</div>
					</div>

					<p class="message-time">Yesterday 12:35 a.m.</p>
					<div class="out-message">
						<div class="initials">
							<p>AA</p>
						</div>
						<div class="message-body">
							<p>That is your business. If you force him to fight, I will come there and show you the real meaing of peacefull negotiation</p>
						</div>
					</div>

				</div>

				<div id="enter-message" class="row">

					<input type="text" class="form-control" id="enter-message-input" placeholder="Type message">
					<img id="send-message-image" src="/images/icons/send.png" alt="" />
					
				</div>


			</div>

		</div>
	</div>

	@include('partials.dashboard.school_sidebar')
	
</div>
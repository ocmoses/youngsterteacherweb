<div class="item-content row" id="subject-time-table-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane">
						
			<div id="time-table-header">
				<h4>Time Table</h4>
				<p>16 Sept - 20 Sept 2019 <small>Third Term</small></p>
				<img id="penguin-image" src="/images/icons/penguin.png" alt="penguin image" />
			</div>

			<div id="time-table-div">

				<table class="table">
				  <thead>
				    <tr>
				      <th scope="col"></th>
				      <th scope="col">9am</th>
				      <th scope="col">10am</th>
				      <th scope="col">11am</th>
				      <th scope="col">12pm</th>
				      <th scope="col">1pm</th>
				      <th scope="col">2pm</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>Monday</td>
				      <td>Mathematics</td>
				      <td>English</td>
				      <td>Health Science</td>
				      <td>Civic</td>
				      <td>Music</td>
				      <td>Civic</td>
				    </tr>
				    <tr>
				      <td>Tuesday</td>
				      <td>English</td>
				      <td>Drawing</td>
				      <td>Writing</td>
				      <td>Music</td>
				      <td>Writing</td>
				      <td>English</td>
				    </tr>
				    <tr>
				      <td>Wednesday</td>
				      <td>Mathematics</td>
				      <td>English</td>
				      <td>Craft</td>
				      <td>Social Studies</td>
				      <td>English</td>
				      <td>Mathematics</td>
				    </tr>
				    <tr>
				      <td>Thursday</td>
				      <td>Civic</td>
				      <td>Craft</td>
				      <td>English</td>
				      <td>Writing</td>
				      <td>Mathematics</td>
				      <td>Music</td>
				    </tr>
				    <tr>
				      <td>Friday</td>
				      <td>Social Studies</td>
				      <td>Craft</td>
				      <td>Social Studies</td>
				      <td>English</td>
				      <td>Music</td>
				      <td>Writing</td>
				    </tr>
				    <tr>
				      <td>Saturday</td>
				      <td>Break</td>
				      <td>Break</td>
				      <td>Break</td>
				      <td>Break</td>
				      <td>Break</td>
				      <td>Break</td>
				    </tr>
				  </tbody>
				</table>

			</div>
		</div>
	</div>
	
	@include('partials.dashboard.school_sidebar')
	
</div>
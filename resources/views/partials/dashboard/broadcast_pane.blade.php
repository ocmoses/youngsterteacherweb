<div class="item-content row active" id="broadcast-pane" >
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">

		<div class="pane" >
			<div style="position: relative; text-align: center; height: 50px;">
				<div class="divider"></div>
				<p style="background-color: white; display: inline-block; padding: 0px 0px;">Today</p>
			</div>

			<div class="broadcast-item">
				<h6>New Assembly Time</h6>
				<p>Student should report to the assembly ground before they wake up from sleep</p>
			</div>					

			<div class="broadcast-item">
				<h6>Morning Exercise</h6>
				<p>Students who refuse to do the madatory mornng exercise will be shot or hanged or both</p>
			</div>

			<div class="broadcast-item ">
				<h6>Teachers and parents</h6>
				<p>All teachers and parents must switch places from Monday. Parents will teach henceforth</p>
			</div>

			<div class="broadcast-item active">
				<h6>Sport Activities</h6>
				<p>Sport activities must stop immediately you hear the whistle or face death</p>
			</div>

			<div class="broadcast-item">
				<h6>Extracurricular Activities</h6>
				<p>Every teacher who does not shoot students who fail to participate in extracurricular activities will be sentenced to death</p>
			</div>

			<div class="broadcast-item">
				<h6>Let's scroll</h6>
				<p>I added this for scrolling to be possible</p>
			</div>			
			
		</div>

	</div>

	@include('partials.dashboard.school_sidebar')
</div>
<div class="item-content row" id="attendance-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane" >
			<div id="attendance-date">oct 23, 2018, 07:50am(Today) </div>

			<div id="child-attendance" class="row">

				<div class="child-attendance-column col-md-6 col-lg-6 col-sm-6">
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/boy.png" alt="child image" />
						<p class="child-name">Michael Jackson</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Musa's Third Daughter</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>	
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/boy.png" alt="child image" />
						<p class="child-name">Orange Boy</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Melissa Rauch</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>	
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/boy.png" alt="child image" />
						<p class="child-name">Tony Montana</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Madonna</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>	
				</div>

				<div class="child-attendance-column col-md-6 col-lg-6 col-sm-6">
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/boy.png" alt="child image" />
						<p class="child-name">Michael Jackson</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Musa's Third Daughter</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>	
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/boy.png" alt="child image" />
						<p class="child-name">Orange Boy</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Melissa Rauch</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>	
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/boy.png" alt="child image" />
						<p class="child-name">Tony Montana</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-attendance-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Madonna</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>	
				</div>
							
			</div> <!-- End of attendance -->

			<button class="btn btn-primary" id="update-attendance">Submit</button>

		</div>
	</div>	

	@include('partials.dashboard.school_sidebar')
	
</div>
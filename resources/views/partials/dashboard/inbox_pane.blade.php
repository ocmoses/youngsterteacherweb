<div class="item-content row" id="inbox-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane" >						
			<ul class="nav nav-tabs" id="messagesTab" role="tablist">

			  <li class="nav-item">
			    <a class="nav-link active" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="true">Message</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="notifications" aria-selected="false">Notifications</a>
			  </li>
			  
			</ul>
			<div class="tab-content" id="tabContent">
			  <div class="tab-pane fade show active" id="messages" role="tabpanel" aria-labelledby="messages-tab">
			  	<div  class="single-message row">
			  		<div class="col-md-1 col-lg-1 col-sm-1 text-center">
			  			<input class="form-check-input" type="checkbox" value="" id="select-message">
			  			<img src="/images/icons/star_selected.png" alt="importance image" />
			  		</div>
			  		
			  		<div class="col-md-2 col-lg-2 col-sm-2 sender-name" class="">
			  			<p>School Admin</p>
			  		</div>
			  		<div class="col-md-2 col-lg-2 col-sm-2 message-title">
			  			<p>New Assembly Time</p>
			  		</div>
			  		<div class="col-md-6 col-lg-6 col-sm-6 message-excerpt">
			  			<p>As we previously stated, every student must report to the assembly ground before the wake up</p>
			  		</div>
			  		<div class="col-md-1 col-lg-1 col-sm-1 message-delete" title="Delete message">
			  			<i class="fas fa-trash"></i>
			  		</div>
			  	</div>
			  	<div  class="single-message row">
			  		<div class="col-md-1 col-lg-1 col-sm-1 text-center">
			  			<input class="form-check-input" type="checkbox" value="" id="select-message">
			  			<img src="/images/icons/star_selected.png" alt="importance image" />
			  		</div>
			  		
			  		<div class="col-md-2 col-lg-2 col-sm-2 sender-name" class="">
			  			<p>School Admin</p>
			  		</div>
			  		<div class="col-md-2 col-lg-2 col-sm-2 message-title">
			  			<p>New Assembly Time</p>
			  		</div>
			  		<div class="col-md-6 col-lg-6 col-sm-6 message-excerpt">
			  			<p>As we previously stated, every student must report to the assembly ground before the wake up</p>
			  		</div>
			  		<div class="col-md-1 col-lg-1 col-sm-1 message-delete" title="Delete message">
			  			<i class="fas fa-trash"></i>
			  		</div>
			  	</div>
			  	<div  class="single-message row">
			  		<div class="col-md-1 col-lg-1 col-sm-1 text-center">
			  			<input class="form-check-input" type="checkbox" value="" id="select-message">
			  			<img src="/images/icons/star_selected.png" alt="importance image" />
			  		</div>
			  		
			  		<div class="col-md-2 col-lg-2 col-sm-2 sender-name" class="">
			  			<p>School Admin</p>
			  		</div>
			  		<div class="col-md-2 col-lg-2 col-sm-2 message-title">
			  			<p>New Assembly Time</p>
			  		</div>
			  		<div class="col-md-6 col-lg-6 col-sm-6 message-excerpt">
			  			<p>As we previously stated, every student must report to the assembly ground before the wake up</p>
			  		</div>
			  		<div class="col-md-1 col-lg-1 col-sm-1 message-delete" title="Delete message">
			  			<i class="fas fa-trash"></i>
			  		</div>
			  	</div>
			  	<div  class="single-message row">
			  		<div class="col-md-1 col-lg-1 col-sm-1 text-center">
			  			<input class="form-check-input" type="checkbox" value="" id="select-message">
			  			<img src="/images/icons/star_selected.png" alt="importance image" />
			  		</div>
			  		
			  		<div class="col-md-2 col-lg-2 col-sm-2 sender-name" class="">
			  			<p>School Admin</p>
			  		</div>
			  		<div class="col-md-2 col-lg-2 col-sm-2 message-title">
			  			<p>New Assembly Time</p>
			  		</div>
			  		<div class="col-md-6 col-lg-6 col-sm-6 message-excerpt">
			  			<p>As we previously stated, every student must report to the assembly ground before the wake up</p>
			  		</div>
			  		<div class="col-md-1 col-lg-1 col-sm-1 message-delete" title="Delete message">
			  			<i class="fas fa-trash"></i>
			  		</div>
			  	</div>
			  	<div  class="single-message row">
			  		<div class="col-md-1 col-lg-1 col-sm-1 text-center">
			  			<input class="form-check-input" type="checkbox" value="" id="select-message">
			  			<img src="/images/icons/star_selected.png" alt="importance image" />
			  		</div>
			  		
			  		<div class="col-md-2 col-lg-2 col-sm-2 sender-name" class="">
			  			<p>School Admin</p>
			  		</div>
			  		<div class="col-md-2 col-lg-2 col-sm-2 message-title">
			  			<p>New Assembly Time</p>
			  		</div>
			  		<div class="col-md-6 col-lg-6 col-sm-6 message-excerpt">
			  			<p>As we previously stated, every student must report to the assembly ground before the wake up</p>
			  		</div>
			  		<div class="col-md-1 col-lg-1 col-sm-1 message-delete" title="Delete message">
			  			<i class="fas fa-trash"></i>
			  		</div>
			  	</div>
			  </div>
			  <div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
			  	<p>Notifications tab</p>
			  </div>
			</div>
		</div>
	</div>

	@include('partials.dashboard.school_sidebar')
	
</div>
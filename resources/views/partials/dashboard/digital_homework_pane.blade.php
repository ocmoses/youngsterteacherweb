<div class="item-content row" id="digital-homework-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane row" >
						
			<div class="col-md-5 col-lg-5 col-sm-5 homework-recipient-list">
				<h5 id="choose-recipient-header">Choose Recipient</h5>

				<p id="current-time">10:00am</p>

				<input type="text" id="recipient-search" placeholder="Type student's name or select from list" />

				<div id="child-recipient-list">

					<div class="child-recipient-item">
						<img class="child-image" src="/images/icons/boy.png" alt="child image" />
						<p class="child-name">Tony Montana</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-recipient-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Madonna</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-recipient-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Madonna</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-recipient-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Madonna</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-recipient-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Madonna</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>
					<div class="child-recipient-item">
						<img class="child-image" src="/images/icons/girl.png" alt="child image" />
						<p class="child-name">Madonna</p>
						<img class="attendance-status" src="/images/icons/check.png" alt="" />
					</div>	

				</div>
				
			</div>
			<div class="col-md-7 col-lg-7 col-sm-7 homework-detail">

				<div id="homework-history-subpane">

					<div id="homework-navigation">
						<i id="back-to-set-assignment" class="fas fa-arrow-left"></i>
					</div>

					<div id="teacher-homework-history">

						<div class="homework-list-item active">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Mathematics Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Agriculture Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Mathematics Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Social Studies Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Civics Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Home Economics Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Health science Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
						<div class="homework-list-item">
							<div class="school-image">
								<img  src="/images/school/school.png" alt="School image" />
							</div>
							<div class="homework-details">
								<h6 class="subject-title">Primary science Homework</h6>
								<p class="assignment-header">Page 45 of that english textbook do number five square root</p>
								<small class="time-set tiny">23 Dec, 2019. 7.00 a.m.</small>
								<small class="time-left tiny">Time left: 2:23:34</small>
							</div>
						</div>
					</div>

				</div>
				
				<div id="homework">
					<div id="set-homework-top">

						<i id="teacher-view-homework-history" class="fas fa-bars"></i>

						<div id="assignment-date-div">
							<label for="set-assignment-date" id="set-deadline">Set deadline</span><br>
							<input type="date" class="set-assignment-date forms-ext" id="set-assignment-date" />
						</div>
						

					</div>
					<input type="text" class="form-control homework-input" id="homework-subject" placeholder="Subject Title">
					<div style="position: relative;">
						<input type="text" class="form-control homework-input" id="homework-desc" placeholder="Enter Description here">
						<img  src="/images/icons/snap.png" alt="" />
					</div>

					<img id="send-assignment-image" src="/images/icons/send.png" alt="" />
				</div>
			</div>
		</div>
	</div>
	
	@include('partials.dashboard.school_sidebar')

</div>
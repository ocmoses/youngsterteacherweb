<div class="item-content row" id="teacher-status-pane">
	
	<div class="col-lg-10 col-md-10 col-sm-12 content-pane">
		<div class="pane">
						
			<div class="select-status-item">
				<img class="status-image" src="/images/icons/free.png" alt="status-image" />
				<p class="status">Free</p>
				<input class="select-status-radio" type="radio" name="select-status-radio" aria-label="Radio button">
			</div>
			<div class="select-status-item">
				<img class="status-image" src="/images/icons/busy.png" alt="status-image" />				
				<p class="status">Busy</p>
				<input class="select-status-radio" type="radio" name="select-status-radio" aria-label="Radio button">
			</div>

			<button class="btn btn-primary view-child">OK</button>

		</div>
	</div>
	
	@include('partials.dashboard.school_sidebar')
	
</div>
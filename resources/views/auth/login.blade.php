@extends('partials.master')

@section('navigation')
@include('partials.main_navigation')
@endsection

@section('content')
<div class="container" style="height: 100vh; width: 90% !important; display: block;">
    <div class="row ">        
        <div class="col-md-4" style="text-align: left; height: 100vh; ">
                       
            <form method="POST" action="{{ route('login') }}" class="login_form login" onsubmit="event.preventDefault(); window.location = '/dashboard';">
                @csrf

                 <div class="form-group row">

                    <img src="/images/logo.png" alt="logo image"/>

                </div>

                <div class="form-group row" style="padding: 0px 32px;">
                    <!-- <label for="email" class="col-md-2 col-form-label text-md-left">{{ __('Email') }}</label> -->

                    <div class="col-md-12">
                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{'teacher@email.com'}}" required autocomplete="email" autofocus placeholder="{{ __('Email or Phone number') }}">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                
                <div class="form-group row" style="padding: 0px 32px;">
                    <!-- <label for="password" class="col-md-2 col-form-label text-md-left">{{ __('Password') }}</label> -->

                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" value="password" placeholder="{{ __('Password') }}">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row" style="padding: 0px 32px;">
                    <div class="col-md-5">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-7 pull-right" style="text-align: right;">
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}" style="text-align: right;">
                                {{ __('Forgot Password?') }}
                            </a>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0" style="padding: 0px 32px;">
                    <div class="col-md-8">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>                       
                    </div>
                </div>
            </form>
               
    </div>
    <div class="col-md-8" style="background: white url('/images/homepage_piture.png') no-repeat right; height: 100vh; background-size: cover; margin-top: 50px; ">

    </div>
</div>
@endsection

@section('footer')

@endsection

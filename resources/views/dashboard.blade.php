@extends('partials.master')

@section('title')

{{ Auth::user() . ' - ' ?? ' - ' }} Dashboard 

@endsection

@section('navigation')
@include('partials.main_navigation')
@endsection

@section('content')
<section class="row dashboard">
	<div class="col-lg-2 col-md-2 dashboard-nav" 
			style="margin: 0px !important; padding: 0px !important;" >
		<div class="dashboard-menu dashboard-header">
			<div class="menu-div">
				<img id="profile-image" src="/images/profiles/man.png" alt="profile picture" />
				<div id="user-div">
					<p id="username">Mr Musa</p>
					<p  id="teaching-status"><img style="width: 10px; margin-top: 0px;" src="/images/icons/red.png" alt="" /> Teaching</p>
				</div>
				
				<span ><i id="menu-icon" class="fas fa-ellipsis-v"></i></span>
				
				<div class="dashboard-popup">
					<div class="popup-top">
						<div id="popup-profile">
							<img  id="popup-profile-image" src="/images/profiles/man.png" alt="profile picture" />
						</div>
						<div id="user-details">
							<p id="popup-username">Mr Musa</p>
							<p id="popup-email">musa@musa.com</p>
							<a href="#" id="edit-email">Edit email</a>
							<a href="#" id="reset-password">Reset Password</a>
						</div>							
					</div>					
					<div class="popup-bottom">
						<button class="popup-button">Support</button>
						<button class="popup-button">Signout</button>
					</div>
				</div>
				
			</div>
		</div>
		<nav class="dashboard-nav-items">
			<ul id="dashboard-nav-items-list">
				<li class="dashboard-nav-item active" data-target="broadcast-pane">
					<img src="/images/icons/notification.png" alt="dashboard icon"/> 
					<!-- <a href="/dashboard/broadcast">Broadcast</a> -->
					Broadcast
				</li>
				<li class="dashboard-nav-item" data-target="attendance-pane">
					<img src="/images/icons/attendance.png" alt="dashboard icon"/> 
					<!-- <a href="/dashboard/attendance">Attendance</a> -->
					Attendance
				</li>
				<li class="dashboard-nav-item" data-target="child-location-pane">
					<img src="/images/icons/location.png" alt="dashboard icon"/> 
					<!-- <a href="/dashboard/child-location">Child Location</a> -->
					Child Location
				</li>
				<li class="dashboard-nav-item"  data-target="chats-pane">
					<img src="/images/icons/chat.png" alt="dashboard icon"/> 
					<!-- <a href="/dashboard/chats">Chats</a> -->
					Chats
				</li>

				<div class="dropdown-divider"></div>

				<li class="dashboard-nav-item"  data-target="inbox-pane">
					<img src="/images/icons/inbox.png" alt="dashboard icon"/> 
					<!-- <a href="/dashboard/inbox">Inbox</a> -->
					Inbox
				</li>
				<li class="dashboard-nav-item"  data-target="digital-homework-pane">
					<img src="/images/icons/homework.png" alt="dashboard icon"/> 
					<!-- <a href="/dashboard/digital-homework">Digital Homework</a> -->
					Digital Homework
				</li>
				<li class="dashboard-nav-item"  data-target="subject-time-table-pane">
					<img src="/images/icons/subject.png" alt="dashboard icon"/> 
					<!-- <a href="/dashboard/subject-time-table">Subject Time Table</a> -->
					Subject Time Table
				</li>
				<li class="dashboard-nav-item" data-target="teacher-status-pane">
					<img src="/images/icons/smiley_face.png" alt="dashboard icon"/> 
					<!-- <a href="/dashboard/select-child">Select Child</a> -->
					Teacher Status
				</li>
			</ul>
		</nav>
	</div>
	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 dashboard-content" style="margin: 0px !important; padding: 0px !important;">
		<div class="dashboard-header dashboard-top-header"></div>
				
			@include('partials.dashboard.broadcast_pane')
		
			@include('partials.dashboard.attendance_pane')
		
			@include('partials.dashboard.child_location_pane')	
		
			@include('partials.dashboard.inbox_pane')
				
			@include('partials.dashboard.teacher_status_pane')
		
			@include('partials.dashboard.subject_time_table_pane')
		
			@include('partials.dashboard.digital_homework_pane')
		
			@include('partials.dashboard.chats_pane')
		
	</div>
	
</section>
@endsection

@section('footer')
	@include('partials.footer')
@endsection
